# DEMO: Deploy k3s (kubernetes) to VirtualBox VM using Ansible

> This is a proof-of-concept demo. (Not for production use!)
>
> HTTPS is done using self-signed authority and passwords are available in the ansible files.

## Pre-requisite:

- Install VirtualBox and allow the current user access to it: `apt install virtualbox`
- Install Packer: `apt install packer`
- Install Packer plugin: `packer plugins install github.com/hashicorp/virtualbox`
- Install Ansible: `apt install ansible`
- Set up VirtualBox Host-Only Network with 192.168.56.0/24 and DHCP server

![VirtualBox Host-Only Network Settings](images/virtualbox_network.png)

## Step 00 - Clone this repository

```bash
git clone https://gitlab/my/repo ansibledemo
```
  * Navigate into the ansibledemo directory

## Step 01

  * Create installation media for Ubuntu Server and then instantiate it.
     * This takes a long time. Go make a coffee. Come back after lunch (for my machine about 10 mins)!

```bash
ansible-playbook playbook/01_install-vm.yml
```
![01_install-vm.gif](images/01_install-vm.gif)

## Step 02

  * Install k3s Kubernetes inside the VM

```bash
ansible-playbook playbook/02_install-k3s.yml
```
![02_install-k3s.gif](images/02_install-k3s.gif)

  * See instructions on what to add to the hosts file to get access to demo apps

```bash
ansible-playbook playbook/open-demo-urls-in-browser.yml
```
![open-demo-urls-in-browser.gif](images/open-demo-urls-in-browser.gif)
## Step 03

  * Install services for this demo cluster

```bash
ansible-playbook playbook/03_install-tools-and-prepare-cluster.yml
```
![03_install-tools-and-prepare-cluster.gif](images/03_install-tools-and-prepare-cluster.gif)
## Step 04
  * Use ArgoCD platform to deploy "guestbook" using CI/CD paradigm.

```bash
ansible-playbook playbook/04_deploy-ci-cd-app-to-cluster.yml
```
![04_deploy-ci-cd-app-to-cluster.gif](images/04_deploy-ci-cd-app-to-cluster.gif)
## Step XX

  * See instructions on what to add to the hosts file to get access to demo apps

```bash
ansible-playbook playbook/open-demo-urls-in-browser.yml
```
  * Check if VM is online. If not, then start it.

```bash
ansible-playbook playbook/vm-online.yml
```

  * Show credentials for ArgoCD

```bash
cat playbook/vars/shared.yml | grep argo
```

  * Access VM with ssh

```bash
ssh demouser@127.0.0.1 -p 2222 -i cache/vms/K3SDemo/ssh/id_rsa_K3SDemo
```


## To Remove
 * Remove lines with argocd.vm and guestbook.vm from /etc/hosts
 *  Remove VirtualBox VM with disks
 *  Remove cloned directory
 *  Remove SSH host key: `ssh-keygen -R '[127.0.0.1]:2222'`
